<?php include 'includes/header.php'; ?>
<?php
        //Create DB object
    $db = new Database();
        //Create query
	$query = "SELECT posts.*, categories.name FROM posts INNER JOIN categories ON posts.category = categories.id";
        //Run query
    $posts = $db->select($query);
         //now we can use this var

    $query = "SELECT * FROM categories";
    $categories = $db->select($query);
	
?>
<?php if($posts) : ?>
    <?php while($row = $posts->fetch_assoc()) : ?>
        <div class="post post-image">
            <div class="post-head"><h3><a href="post.php?id=<?php echo urlencode($row['id']); ?>"><?php echo $row['title']; ?></a></h3><!--date isnt nice so it will our first helper function to format date-->
                <div class="post-meta">
                    <div><i class="fa fa-user"></i>BY : <a href="#"><?php echo $row['author']; ?></a></div>
                    <div><i class="fa fa-clock-o"></i><?php echo formatDate($row['date']); ?></div>
                    <!--<div><i class="fa fa-comments"></i><a href="#">22 Comments</a></div>-->
                    <div><i class="fa fa-folder-open"></i>IN : <a href="#"><?php echo $row['name']; ?></a></div>
                    <!--<div><i class="fa fa-heart"></i><a href="#">77</a></div>-->
                </div><!-- End post-meta -->
                <div class="clearfix"></div>
            </div><!-- End post-head -->
            <div class="post-wrap">
                <div class="post-img"><a href="single-blog.html"><img alt="" src="..."></a></div>
                <div class="post-inner">
                    <p><?php echo shortenText($row['body']); ?></p>

                    <div class="clearfix"></div>
                    <div class="post-share-view">
                        <div class="post-meta">
							<div><i class="fa fa-tags"></i><?php echo $row['tags']; ?></div>
                            <!--<div><i class="fa fa-eye"></i><span>145 </span>Views</div>-->
                            <!--<div class="post-meta-share">
                                <i class="fa fa-share-alt"></i>
                                <a href="#">Share This</a>
                                <div class="share-social">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                    <i class="fa fa-caret-down"></i>
                                </div>
                            </div>-->

                            <a class="button post-more" href="post.php?id=<?php echo urlencode($row['id']); ?>">Read More</a>

                        </div><!-- End post-meta -->
                    </div><!-- End post-share-view -->
                    <div class="clearfix"></div>
                </div><!-- End post-inner -->
            </div><!-- End post-wrap -->
        </div><!-- End post -->
    <?php endwhile; ?>
<?php else : ?>
    <p>There are no posts there</p>
<?php endif; ?>


<?php include 'includes/footer.php'; ?>


