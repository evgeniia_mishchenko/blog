<?php include 'includes/header.php'; ?>
<?php
$db = new Database();
if(isset($_POST['submit'])){
    //Assign vars
    $title = mysqli_real_escape_string($db->link, $_POST['title']);
    $body = mysqli_real_escape_string($db->link, $_POST['body']);
    $category = mysqli_real_escape_string($db->link, $_POST['category']);
    $author = mysqli_real_escape_string($db->link, $_POST['author']);
    $tags = mysqli_real_escape_string($db->link, $_POST['tags']);
    //Simple validation
    if($title == '' || $body == '' || $category == '' || $author == ''){
        //Set error
        $error = 'Please fill out all required fields';
    } else {
        $query = "INSERT INTO posts (title, body, category, author, tags)
                                VALUES ('$title', '$body', '$category' ,'$author' ,'$tags')";
        $insert_row = $db->insert($query);
    }
}
$query = "SELECT * FROM categories";
$categories = $db->select($query);
?>
    <div class="col-md-2"></div>
    <div class="col-md-8 main-content">


        <form  method="POST" action="add_post.php">
            <div class="form-group">
                <label for="title">Post Title: </label>
                <input type="text" name="title" id="title" class="form-control"  placeholder="Enter Title" />
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="body">Post Body: </label>
                <textarea name="body" id="body" class="form-control" rows="10" placeholder="Enter Post Body"></textarea>
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="category">Category: </label>
                <select name="category" id="category" class="form-control">
                    <?php while($row = $categories->fetch_assoc()) : ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name'] ?></option>
                    <?php endwhile; ?>
                </select>



            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="author">Author: </label>
                <input type="text" name="author" id="author" class="form-control"  placeholder="Enter Author Name" />
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="author">Tags: </label>
                <input type="text" name="tags" id="author" class="form-control"  placeholder="Enter Tags" />
            </div>

            <input type="submit" name="submit" class="btn" id="success" value="Submit" />
            <a href="index.php" class="btn btn-default" role="button" id="cancel">Cancel</a>
        </form>
    </div>
    <div class="col-md-2"></div>

<?php include 'includes/footer.php'; ?>