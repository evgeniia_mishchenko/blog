<?php include 'includes/header.php'; ?>
<?php
    $id = $_GET['id'];
    $db = new Database();
    $query = "SELECT * FROM posts WHERE posts.id = ".$id;
    $post = $db->select($query)->fetch_assoc();
    $query = "SELECT * FROM categories";
    $categories = $db->select($query);

?>
<?php
if(isset($_POST['submit'])){
    //Assign vars
    $title = mysqli_real_escape_string($db->link, $_POST['title']);
    $body = mysqli_real_escape_string($db->link, $_POST['body']);
    $category = mysqli_real_escape_string($db->link, $_POST['category']);
    $author = mysqli_real_escape_string($db->link, $_POST['author']);
    $tags = mysqli_real_escape_string($db->link, $_POST['tags']);
    //Simple validation
    if($title == '' || $body == '' || $category == '' || $author == ''){
        //Set error
        $error = 'Please fill out all required fields';
    } else {
        $query = "UPDATE posts SET
                            title = '$title',
                            body = '$body',
                            category = '$category',
                            author = '$author',
                            tags = '$tags'
                            WHERE id =".$id;
        $update_row = $db->update($query);
    }
}
$query = "SELECT * FROM categories";
$categories = $db->select($query);
?>
<?php
    if(isset($_POST['delete'])){
        $query = "DELETE FROM posts WHERE id = ".$id;
        $delete_row = $db->delete($query);
        //Call delete method

    }
?>

    <div class="col-md-2"></div>
    <div class="col-md-8 main-content">

        <form method="POST" action="edit_post.php?id=<?php echo $id; ?>">
            <div class="form-group">
                <label for="title">Post Title: </label>
                <input type="text" name="title" id="title" class="form-control"  placeholder="Enter Title" value="<?php echo $post['title']; ?>">
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="body">Post Body: </label>
                <textarea name="body" id="body" class="form-control" rows="10" placeholder="Enter Post Body"><?php echo $post['body']; ?></textarea>
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="category">Category: </label>
                <select name="category" id="category" class="form-control">
                    <?php while($row = $categories->fetch_assoc()) : ?>
                        <?php
                            if($row['id'] == $post['category']){
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            }
                        ?>
                        <option <?php echo $selected; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php endwhile; ?>
                </select>
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="author">Author: </label>
                <input type="text" name="author" id="author" class="form-control"  placeholder="Enter Author Name" value="<?php echo $post['author']; ?>">
            </div>
            <div class="form-group" method="POST" action="add_post.php">
                <label for="author">Tags: </label>
                <input type="text" name="tags" id="author" class="form-control"  placeholder="Enter Tags" value="<?php echo $post['tags']; ?>">
            </div>

            <input type="submit" name="submit" class="btn" id="success" value="Edit" />
            <a href="index.php" class="btn btn-default" role="button" id="cancel">Cancel</a>
            <input type="submit" name="delete" class="btn btn-danger pull-right" value="Delete" />

        </form>
    </div>
    <div class="col-md-2"></div>

<?php include 'includes/footer.php'; ?>