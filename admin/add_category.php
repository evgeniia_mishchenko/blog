<?php include 'includes/header.php'; ?>
<?php
    $db = new Database();
    if(isset($_POST['submit'])){
        //Assign vars
        $name = mysqli_real_escape_string($db->link, $_POST['name']);
        //Simple validation
        if($name == ''){
        //Set error
        $error = 'Please fill out all required fields';
        } else {
        $query = "INSERT INTO categories (name) VALUES ('$name')";
        $insert_row = $db->insert($query);
        }
    }
?>
    <div class="col-md-2"></div>
    <div class="col-md-8 main-content">


    <form method="POST" action="add_category.php">
        <div class="form-group">
            <label for="category">Category Name: </label>
            <input type="text" name="name" class="form-control" id="category" placeholder="Enter Category">
        </div>

        <input type="submit" name="submit" class="btn" id="success" value="Submit" />
        <a href="index.php" class="btn btn-default" role="button" id="cancel">Cancel</a>
    </form>

    </div>
    <div class="col-md-2"></div>

<?php include 'includes/footer.php'; ?>