<?php include '../config/config.php'; ?>   <!-- config should be in the top -->
<?php include '../libraries/Database.php'; ?>    <!--its not gonna do anything untill we actually create a DB object-->
<?php include '../helpers/format_helper.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://faviconka.ru/ico/faviconka_ru_67.ico">

    <title>Admin Area</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link href="../css/blog.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>

<body>

<style>
    input[type="text"] {background-color: #FFF;}
    #body, #category {
        border: 1px solid #E4E6E8;
        color: #9BA1A8;
        font-size: 12px;
    }
    #success {background-color: #5CB85C;}
    #success:hover {
        background-color: #293545;
        color: #FFF;
    }
    #cancel {
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;

</style>

<div id="wrap" class="stripe grid_1200">

    <div class="wrap-search">
        <form method="post">
            <input type="search" name="s" value="Search here ..." onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
        </form>
    </div><!-- End wrap-search -->

    <div class="progress" style="height: 3px; margin-bottom: 0; border: none;">
        <div class="progress-bar progress-bar-success" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 5%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 7%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 6%; height: 3px;"></div>
    </div>

    <header id="header">
        <div class="container clearfix">
            <div class="demo_navigation">
                <div class="push_options"><a class="show_hide"><i class="fa fa-2x fa-spin fa-cog"></i></a></div>
            </div>
            <div class="logo"><a href="index.php">Admin Area</a></div><!-- End logo -->
            <div class="header-follow">
                <a href="../index.php"><div class="header-follow-a">Visit Blog</div></a>

            </div><!-- End header-follow -->

            <div class="header-search">
                <div class="header-search-a"><i class="fa fa-search"></i></div>
            </div><!-- End header-search -->
            <nav class="navigation">
                <ul>
                    <li class="current_page_item mega-menu"><a href="index.php"><strong>Dashboard</strong></a></li>
                    <li><a href="add_post.php">Add Post  <i class="fa fa-plus"></i></a></li>
                    <li><a href="add_category.php">Add Category  <i class="fa fa-plus"></i></a></li>
                </ul>
            </nav><!-- End navigation -->
            <nav class="navigation_mobile navigation_mobile_main">
                <div class="navigation_mobile_click">Go to...</div>
                <ul></ul>
            </nav><!-- End navigation_mobile -->
        </div><!-- End container -->
    </header><!-- End header -->

    <div class="clearfix"></div>
    <div class="sections">
        <div class="container">
            <div class="row">