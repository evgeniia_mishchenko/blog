
        </div><!-- End row -->
    </div><!-- End container -->
</div><!-- End sections -->

<div class="clearfix"></div>

<footer id="footer">
    <div class="container">
        <div class="copyrights">Copyright 2015 <a href="index.php">PHPLoversBlog </a></div>
        <div class="social-ul">
            <ul>
                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li class="social-youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                <li class="social-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul>
        </div><!-- End social-ul -->
    </div><!-- End container -->
</footer><!-- End footer -->

</div><!-- End wrap -->
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.js"></script>

</body>
</html>