<?php include 'includes/header.php'; ?>
<!--here we will need to connect to our Database class and pull posts and categories right in our dashboard -->
<?php
    $db = new Database();
        //here we will need to join categories table to get the category name otherwise from posts table we could only get category ID
        //we want to link primary key from categories which is id to the foreign category in posts table
        //all we want to select fron the category table is the name
    $query = "SELECT posts.*, categories.name FROM posts INNER JOIN categories ON posts.category = categories.id ORDER BY posts.title DESC";
    $posts = $db->select($query);
    $query = "SELECT * FROM categories ORDER BY name";
    $categories = $db->select($query);
?>

<div class="col-md-12 main-content">
    <div class="table-responsive">
        <table class="table table-hover ">
            <tr class="active">
                <th class="text-center"><strong>Post ID#</strong></th>
                <th><strong>Post Title</strong></th>
                <th><strong>Category</strong></th>
                <th><strong>Author</strong></th>
                <th><strong>Date</strong></th>
            </tr>
            <?php while( $row = $posts->fetch_assoc()) : ?>
                <tr>
                    <td class="text-center active"><?php echo $row['id']; ?></td>
                    <td><a href="edit_post.php?id=<?php echo $row['id']; ?>" data-toggle="tooltip" title="Edit"><?php echo $row['title']; ?>  <i class="fa fa-pencil-square-o"></i></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['author']; ?></td>
                    <td><?php echo formatDate($row['date']); ?></td>
                </tr>
            <?php endwhile; ?>

        </table>
    </div>

    <div class=" ">
        <table class="table table-hover ">
            <tr class="active">
                <th class="text-center"><strong>Category ID#</strong></th>
                <th><strong>Category Name</strong></th>
            </tr>
            <?php while($row = $categories->fetch_assoc()) : ?>
                <tr>
                    <td class="text-center active"><?php echo $row['id']; ?></td>
                    <td><a href="edit_category.php?id=<?php echo $row['id']; ?>" data-toggle="tooltip" title="Edit"><?php echo $row['name']; ?>  <i class="fa fa-pencil-square-o"></i></td>
                </tr>
            <?php endwhile; ?>

        </table>
    </div>
</div>


<?php include 'includes/footer.php'; ?>

