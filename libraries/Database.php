<!-- Now we are talking about OOP. In OOP u have class and u can simply build objects from it.
 e.g. :
<?php

class Dog {
    public $color;      //its a property. we can assign it a value right here - default value
    public function bark(){
        echo 'Bark!';
    }
}

//there are 3 levels of access-identifiers: private/protected/public
//public: even if we not working within this Dog class or we in another file..we can access this $color attribute
//protected: only from that classes and any classes that extend Dog class
//private: only in this class
//At this point we just defining a class/
//To use class we most likely gonna be in another file. we not gonna use it here/
// Its just a library where we define staff

/*
 *      $dog = new Dog;
 *      $dog->color = 'black';
 *      $dog->bark();
*/
?>



<?php

class Database {
    public $host = DB_HOST; //we can simply put actual values here but its not a good practice
    public $username = DB_USER;
    public $password = DB_PASS;
    public $db_name = DB_NAME;

    public $link;   //gonna represent msqli object
    public $error;

    /*
     * Class Constructor
     */

    public function __construct(){

        //Call Connect Function
        $this->connect();     //if u r in function which in class and u want to call another function of this class simply use $this
    }

    /*
     * Connector
     */
    private function connect(){   //because we will call just from our class in constructor function
        $this->link = new mysqli($this->host, $this->username, $this->password, $this->db_name);    //basically this is just a connect
                        //then we gonna make sure that we have connected
        if(!$this->link){
            $this->error = "Connection Failed: ".$this->link->connect_error;    //this will give us an actual MySQL error
            return false;
        }
    }
    /*this functionality only be available through the admin page*/
    /*
     * SELECT
     */

    public function select($query){
        $result = $this->link->query($query) or die($this->link->error.__LINE__);  //$this->link refers to mysqli object. this means that whatever we can do with this object we can do with this var
        if($result->num_rows > 0){
            return $result;
        } else {
            return false;
        }
    } //if we look at other DB classes(mysqli or PDO) they are really complicated comparing with this one

    /*
     * INSERT
     */
    public function insert($query){
        $insert_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate insert
        if($insert_row){        //if true - redirect to admin index page
            header('Location: http://localhost/studying/10projects/3_Blog/admin/index.php?msg='.urlencode('Record Added'));
                //when we redirect after we insert it we wonna have a message to display
                // eg we can include GET var inside the URL: Location: index.php?msg=".urlencode('Record Added').
                //and then when we actually redirected we have that var that we can grab and print on the screen
            exit();
        } else {
            die('Error :('. $this->link->errno .') '. $this->link->error);  //if it doesnt insert
        }
    }

    /*
     * UPDATE
     */
    public function update($query){
        $update_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate insert
        if($update_row){        //if true - redirect to admin index page
            header('Location: http://localhost/studying/10projects/3_Blog/admin/index.php?msg='.urlencode('Record Updated'));
            //when we redirect after we insert it we wonna have a message to display
            // eg we can include GET var inside the URL: Location: index.php?msg=".urlencode('Record Added').
            //and then when we actually redirected we have that var that we can grab and print on the screen
            exit();
        } else {
            die('Error :('. $this->link->errno .') '. $this->link->error);  //if it doesnt insert
        }
    }


    /*
     * DELETE
     */
    public function delete($query){
        $delete_row = $this->link->query($query) or die($this->link->error.__LINE__);

        //Validate insert
        if($delete_row){        //if true - redirect to admin index page
            header('Location: http://localhost/studying/10projects/3_Blog/admin/index.php?msg='.urlencode('Record Deleted'));
            //when we redirect after we insert it we wonna have a message to display
            // eg we can include GET var inside the URL: Location: index.php?msg=".urlencode('Record Added').
            //and then when we actually redirected we have that var that we can grab and print on the screen
            exit();
        } else {
            die('Error :('. $this->link->errno .') '. $this->link->error);  //if it doesnt insert
        }
    }


    /*
     * We have simple SELECT/INSERT/UPDATE/DELETE if we need more eg get the total we can add as well
     * NEXT IS JUST TRY TO CONNECT. try to get the posts from the category into our index.page
     */
}

// in class u have a constructor - basically its a function that runs when u instantiate a class
// e.g. $db = new Database;   just by instantiating like this we gonna call a constructor if we have one

?>


