
                    </div><!-- End main-content -->
                    <div class="col-md-4 sidebar">
                        <div class="widget widget-about">
                            <div class="widget-about-img"><img alt="" src="..."></div>
                            <h3>About</h3>
                            <p><?php echo $site_description; ?></p>
                            <div class="social-ul">
                                <ul>
                                    <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="social-youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                    <li class="social-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                                </ul>
                            </div><!-- End social-ul -->
                            <div class="clearfix"></div>
                        </div><!-- End widget -->

                        <div class="widget">
                            <div class="widget-title"><i class="fa fa-search"></i>Search</div>
                            <div class="widget-search">
                                <form>
                                    <input type="search" value="Search here ..." onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
                                    <input class="button" type="submit" value="Search Now">
                                </form>
                            </div><!-- End widget-search -->
                        </div><!-- End widget -->

                        <div class="widget">
                            <div class="widget-title"><i class="fa fa-folder-open"></i>Categories</div>
                            <div class="widget-categories">
                                <?php if($categories) : ?>
                                <ul>
                                    <?php while($row = $categories->fetch_assoc()) : ?>
                                        <li><a href="posts.php?category=<?php echo $row['id']; ?>"> <?php echo $row['name']; ?></a><i class="fa fa-angle-double-right"></i></li>
                                    <?php endwhile; ?>
                                </ul>
                                <?php else : ?>
                                    <p>There is no categories</p>
                                <?php endif; ?>
                            </div><!-- End widget-categories -->
                        </div><!-- End widget -->


                        <div class="widget">
                            <div class="widget-title"><i class="fa fa-lock"></i>Login Area</div>
                            <div class="widget-login">
                                <form>
                                    <input type="text" value="Username" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
                                    <div class="widget-login-password">
                                        <input type="password" value="Password" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
                                        <a href="#">Forget</a>
                                    </div>
                                    <input type="submit" value="Login" class="button">
                                </form>
                            </div>
                        </div><!-- End widget -->
                    </div><!-- End sidebar -->
                    </div><!-- End row -->
                    </div><!-- End container -->
                    </div><!-- End sections -->
        <div class="clearfix"></div>

        <footer id="footer">
            <div class="container">
                <div class="copyrights">Copyright 2015 | <a href="index.php">PHPLoversBlog </a></div>
                <div class="social-ul">
                    <ul>
                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="social-youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="social-rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div><!-- End social-ul -->
            </div><!-- End container -->
        </footer><!-- End footer -->
    </div><!-- End wrap -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
        !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
    </script>   <!--Upload file input script-->

    </body>
</html>