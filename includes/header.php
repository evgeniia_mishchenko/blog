<?php include 'config/config.php'; ?>   <!-- config should be in the top -->
<?php include 'libraries/Database.php'; ?>    <!--its not gonna do anything untill we actually create a DB object-->
<?php include 'helpers/format_helper.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://faviconka.ru/ico/faviconka_ru_67.ico">

    <title>Welcome To PHPLoversBlog</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/base.css" rel="stylesheet">
    <link href="css/blog.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>

<body>
<!--<div class="loader"><i class="loader_html fa-spin"></i></div>   LOAD indicator-->
<div id="wrap" class="stripe grid_1200">
    <div class="wrap-search">
        <form method="post">
            <input type="search" name="s" value="Search here ..." onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
        </form>
    </div><!-- End wrap-search -->
    <div class="progress" style="height: 3px; margin-bottom: 0; border: none;">
        <div class="progress-bar progress-bar-success" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 5%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 7%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 8%; height: 3px;"></div>
        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 6%; height: 3px;"></div>
        <div class="progress-bar progress-bar-danger" style="width: 10%; height: 3px;"></div>
        <div class="progress-bar progress-bar-success" style="width: 6%; height: 3px;"></div>

    </div>
    <header id="header">
        <div class="container clearfix">
            <div class="demo_navigation">
                <div class="push_options"><a class="show_hide"><i class="fa fa-2x fa-spin fa-cog"></i></a></div>
            </div>
            <div class="logo"><a href="index.php">PHP Lovers Blog</a></div><!-- End logo -->
            <div class="header-follow">
                <a href="register.php"><div class="header-follow-a">Create Account</div></a>
                <a href="admin/index.php"><div class="header-follow-a">Admin Area</div></a>

            </div><!-- End header-follow -->

            <div class="header-search">
                <div class="header-search-a"><i class="fa fa-envelope"></i></div>
            </div><!-- End header-search -->
            <nav class="navigation">
                <ul>
                    <li class="current_page_item mega-menu"><a href="index.php">Home<span class="menu-nav-arrow"><i class="fa fa-angle-down"></i> </span></a>
                        <ul>
                            <li class="col-md-3">
                                <ul>
                                    <li class="current_page_item"><a href="index.html">Home 1</a></li>
                                    <li><a href="index-2.html">Home 2</a></li>
                                    <li><a href="index-3.html">Home 3</a></li>
                                    <li><a href="index-4.html">Home 4</a></li>
                                </ul>
                            </li>
                            <li class="col-md-3">
                                <ul>
                                    <li><a href="index-5.html">Home 5</a></li>
                                    <li><a href="index-6.html">Home 6</a></li>
                                    <li><a href="index-7.html">Home 7</a></li>
                                    <li><a href="index-8.html">Home 8</a></li>
                                </ul>
                            </li>
                            <li class="col-md-3">
                                <ul>
                                    <li><a href="index-9.html">Home 9</a></li>
                                    <li><a href="index-dark.html">Home Dark</a></li>
                                    <li><a href="index-boxed.html">Home Boxed</a></li>
                                    <li><a href="index-boxed-2.html">Home Boxed 2</a></li>
                                </ul>
                            </li>
                            <li class="col-md-3">
                                <ul>
                                    <li><a href="index-10.html">Home 10</a></li>
                                    <li><a href="index-11.html">Home 11</a></li>
                                    <li><a href="index-12.html">Home 12</a></li>
                                    <li><a href="index-13.html">Home 13</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="posts.php">All Posts<span class="menu-nav-arrow"><i class="fa fa-angle-down"></i> </span></a>
                        <ul>
                            <li><a href="index.html">Headers</a>
                                <ul>
                                    <li><a href="index.html">Header 1</a></li>
                                    <li><a href="header-2.html">Header 2</a></li>
                                    <li><a href="header-3.html">Header 3</a></li>
                                    <li><a href="header-4.html">Fixed Navigation</a></li>
                                </ul>
                            </li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="authors.html">Authors</a></li>
                            <li><a href="right-sidebar.html">Page Right Sidebar</a></li>
                            <li><a href="left-sidebar.html">Page Left Sidebar</a></li>
                            <li><a href="centered-page.html">Page Centered Sidebar</a></li>
                            <li><a href="full-width.html">Page Full Width</a></li>
                            <li><a href="404.html">404 Page</a></li>
                        </ul>
                    </li>
                    <li><a href="archives.html">Archives</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav><!-- End navigation -->
            <nav class="navigation_mobile navigation_mobile_main">
                <div class="navigation_mobile_click">Go to...</div>
                <ul></ul>
            </nav><!-- End navigation_mobile -->
        </div><!-- End container -->
    </header><!-- End header -->

    <div class="clearfix"></div>
    <div class="sections">
        <div class="container">
            <div class="row">
                <div class="col-md-8 main-content">