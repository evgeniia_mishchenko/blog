<?php
/*
 * Format The Date
 */

function formatDate($date){
    return date('F j, Y, h:i a', strtotime($date));

}
//the we need to include this file and use this function

/*
 * Shorten The Text
 */
function shortenText($text, $chars = 200){      //if not putted parameter it would be 450 by default
    $text = $text." ";
    $text = substr($text, 0, $chars);   // 0: start of string; $chars = length we want
    $text = substr($text, 0, strrpos($text, ' '));   //not to cut text in the middle of the word, strpos() gonna find where there is a space and substr gonna stop there
    $text = $text."...";
    return $text;
}

?>