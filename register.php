<?php include 'includes/header.php'; ?>
<?php
$db = new Database();
$query = "SELECT * FROM categories";
$categories = $db->select($query);

?>
            <div class="main-col">

                <div class="block">
                    <h3>Create An Account</h3>


                    <div class="row">

                            <form class="form-horizontal" id="register-form" role="form" enctype="multipart/form-data" method="POST" action="register.php"> <!--multipart/form-data because we have file upload-->
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="username" class="col-md-3">Choose Username <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" name="name" class="form-control" id="username" placeholder="Create A Username">
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="email1" class="col-md-3">Email address <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="email" name="email" class="form-control" id="email1" placeholder="Enter Your Email Address">
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="password1" class="col-md-3">Password <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="password" name="password" class="form-control" id="password1" placeholder="Enter A Password">
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="password2" class="col-md-3">Confirm Password <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="password" name="password2" class="form-control" id="password2" placeholder="Enter Password Again">
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="avatar" class="col-md-3">Upload Avatar </label>
                                    <div class="col-md-7">

                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" />
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">?</a>
                                        </div>

                                    </div>
                                    <div class="col-md-1"></div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label for="about" class="col-md-3">About Me</label>
                                    <div class="col-md-7">
                                        <textarea name="email" class="form-control" id="about" cols="80" rows="6" placeholder="Tell Us About Yourself (optional)"></textarea>
                                        <input type="submit" name="register" class="btn btn-primary" value="Register">

                                    </div>
                                    <div class="col-md-1"></div>
                                </div>

                            </form>
                    </div>
                </div>


            </div>

<?php include 'includes/footer.php'; ?>